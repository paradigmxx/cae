-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 10, 2020 at 04:03 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cae`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `answer_id` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`answer_id`, `id`, `question_id`, `value`) VALUES
(93, 9, 31, 1),
(94, 9, 32, 2),
(95, 9, 33, 3),
(96, 9, 34, 2),
(125, 6, 31, 1),
(126, 6, 32, 2),
(127, 6, 33, 3),
(128, 6, 34, 2),
(129, 7, 32, 2),
(130, 7, 33, 3),
(131, 7, 34, 1),
(132, 7, 35, 2),
(139, 7, 37, 1),
(140, 7, 38, 3),
(141, 7, 39, 1),
(142, 7, 46, 1),
(143, 7, 47, 1),
(144, 7, 48, 3),
(145, 7, 49, 1);

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `option_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  `o_text` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`option_id`, `question_id`, `value`, `o_text`) VALUES
(1, 14, 1, '1o1q38t'),
(2, 16, 1, '1o3q38t'),
(3, 16, 2, '2o3q38t'),
(4, 17, 1, 'asdf'),
(5, 17, 2, 'sdf'),
(6, 14, 1, 'fsdgdfshsdth'),
(7, 14, 2, 'sdghgsjhdf'),
(8, 14, 1, '+1'),
(9, 15, 1, 'fgdfg'),
(10, 21, 1, 'asd'),
(11, 21, 2, 'ss'),
(12, 22, 1, 'hindi ako'),
(13, 22, 2, 'andrei'),
(14, 23, 1, 'binitay'),
(15, 23, 2, 'binaril'),
(16, 23, 3, 'nilagay sa piso'),
(17, 24, 1, 'bimbitch'),
(18, 24, 2, 'banggay'),
(19, 25, 1, '1'),
(20, 25, 2, '2'),
(21, 26, 1, '1'),
(22, 26, 2, '2'),
(23, 30, 1, 's'),
(24, 30, 2, 'q'),
(25, 31, 1, '1'),
(26, 31, 2, '2'),
(27, 32, 1, 'dfdfg'),
(28, 32, 2, '2nd option'),
(29, 33, 1, '1'),
(30, 33, 2, '2'),
(31, 33, 3, '3'),
(32, 34, 1, 'asd'),
(33, 34, 2, 'ss'),
(34, 35, 1, 'True'),
(35, 35, 2, 'False'),
(36, 36, 1, 'q'),
(37, 36, 2, 's'),
(40, 38, 1, '1'),
(41, 38, 2, '2'),
(42, 38, 3, '3'),
(43, 38, 4, '4'),
(46, 40, 1, 'Yes'),
(47, 40, 2, 'Maybe'),
(48, 40, 3, 'Nah'),
(49, 41, 1, ''),
(50, 41, 2, ''),
(51, 41, 3, ''),
(52, 42, 1, ''),
(53, 42, 2, ''),
(54, 43, 1, 'asd'),
(55, 43, 2, 'asd'),
(56, 44, 1, 'True'),
(57, 44, 2, 'False'),
(58, 45, 1, 'cmx'),
(60, 46, 1, 'francis'),
(61, 46, 2, 'prim'),
(62, 47, 1, 'santiago'),
(63, 47, 2, 'sanchez'),
(64, 47, 3, 'santino'),
(65, 48, 1, 'pangalinan'),
(66, 48, 2, 'paguigan'),
(67, 48, 3, 'pagunuran'),
(68, 48, 4, 'pagalilauan'),
(69, 49, 1, 'True'),
(70, 49, 2, 'False');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `question_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `q_text` varchar(65) NOT NULL,
  `correct` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`question_id`, `test_id`, `q_text`, `correct`) VALUES
(32, 50, '1st question', '1'),
(33, 50, '3 q s', '3'),
(34, 50, '4th question', '2'),
(35, 50, 'and thats youd', '1'),
(39, 53, 'much is truez', '1'),
(40, 54, 'Can I add questions?', '1'),
(42, 53, 'tanong ulit', '1'),
(43, 53, 'add q check opt', '2'),
(44, 53, 'tof', '1'),
(45, 57, 'mc', '1'),
(46, 58, 'what is my name', '1'),
(47, 58, 'what is my middle name', '1'),
(48, 58, 'what is my last name', '3'),
(49, 58, 'totoo ba sinasabi ko', '1');

-- --------------------------------------------------------

--
-- Table structure for table `scores`
--

CREATE TABLE `scores` (
  `score_id` int(11) NOT NULL,
  `id` int(65) NOT NULL,
  `test_id` int(65) NOT NULL,
  `score` int(65) NOT NULL,
  `hanggang` int(65) NOT NULL,
  `name` varchar(65) NOT NULL,
  `testsname` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scores`
--

INSERT INTO `scores` (`score_id`, `id`, `test_id`, `score`, `hanggang`, `name`, `testsname`) VALUES
(10, 9, 50, 4, 4, 'prim p', 'yeees'),
(18, 6, 50, 4, 4, 'user user', 'yeees'),
(19, 7, 50, 1, 4, 'stud ent', 'yeees'),
(22, 7, 53, 3, 3, 'stud ent', 'new test'),
(23, 7, 58, 4, 4, 'stud ent', 'legit na test');

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

CREATE TABLE `tests` (
  `test_id` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `testname` varchar(65) NOT NULL,
  `code` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tests`
--

INSERT INTO `tests` (`test_id`, `id`, `testname`, `code`) VALUES
(45, 7, '2020/02/26g', '5FA='),
(46, 7, '2020/02/26h', 'uBFtOMYmN44onPX9TO4HG/p+43dtV3nVPzf1XRqbm0Jv2OQjJYfOMRO1X0WgGJQyf'),
(53, 6, 'test ko na', '7xF8O4hWcYZ6yqc='),
(54, 6, 'dedesign palang ako huhu', '7xF8O4hWcYZ6yqA='),
(56, 6, 'dagdag', '7xF8O4hWcYZ6yqI='),
(57, 6, 'add ako exam', '7xF8O4hWcYZ6yqM='),
(58, 6, 'legit na test', '7xF8O4hWcYZ6yqw=');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `firstname` text NOT NULL,
  `lastname` text NOT NULL,
  `account_type` int(65) NOT NULL,
  `password` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `firstname`, `lastname`, `account_type`, `password`) VALUES
(1, 'user', 'test', 'test', 1, '9'),
(5, 'user1', 'pass', 'pass', 1, '9'),
(6, 'user2', 'user', 'user', 1, '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684'),
(7, 'user3', 'stud', 'ent', 0, '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684'),
(8, 'user4', 'pass', 'pass', 0, '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684'),
(9, 'user21', 'prim', 'p', 0, '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684'),
(10, 'user10', 'dawn', 'break', 1, '1161e6ffd3637b302a5cd74076283a7bd1fc20d3'),
(11, 'user11', 'teacher', 'po ako', 1, '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`answer_id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`option_id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`question_id`);

--
-- Indexes for table `scores`
--
ALTER TABLE `scores`
  ADD PRIMARY KEY (`score_id`);

--
-- Indexes for table `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`test_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `answer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=146;

--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `question_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `scores`
--
ALTER TABLE `scores`
  MODIFY `score_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `tests`
--
ALTER TABLE `tests`
  MODIFY `test_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
