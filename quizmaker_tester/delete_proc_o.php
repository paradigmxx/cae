<?php
session_start();

include "includes/perfect_function.php";
include "includes/database.php";
include "includes/dc_asset.php";


function delete_o($id, $table_name)
{
	$conn = getConnection();
	$sql = "DELETE FROM $table_name where option_id=$id";
	if ($conn->query($sql) == TRUE) {
		$result = "Record deleted successfully";
	} else {
		$result = "Error: " . $sql . "<br>" . $conn->error;	
	}
	return $result;
}


$table_name = "options";

//get user ID from URL
$id = $_GET['id'];
delete_o($id, $table_name);

header("Location: options.php?question_id=".$_SESSION['q']);

?>