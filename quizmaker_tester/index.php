<?php
session_start();
if (isset($_SESSION['alert_msg'])){
    if ($_SESSION['alert_msg']==1){
        echo "Username or password incorrect. Please try again.";
        session_unset();
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>CAE</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src='main.js'></script>
</head>
<body>

<br><br><br>

<div class="card mb-4"  style="width:50%; margin: 0 auto; padding-bottom:30px;" align=left><!-- / -->
<br><br><br><br>
<h1 style="height: bold; font-size: 70px; margin-top: -45px; margin-left: 3%; font-family: helvetica; color:#040404;" align=left><b>LOG IN</b><h1>
<hr style="margin-bottom: 50px;">
<form method="post" action="login_proc.php">
    <input type=text placeholder=Username name=username class="form-control form-control-user" autocomplete=off required style="width:60%; margin-left:3%; margin-top:1%;"> 
        
    <input type=password placeholder=**** name=password class="form-control form-control-user" autocomplete=off required style="width:60%; margin-left:3%; margin-top:1%;"> 

    <button style="margin-left: 3%;" type=submit class="btn btn-success"> ENTER</button>
	<a href="signup.php" class="btn btn-outline-secondary"><span>SIGN UP</span></a>
</form>

    
</div>
</div>
</body>
</html>