<?php
session_start();

include "includes/perfect_function.php";
include "includes/database.php";
include "includes/header.php";


$_SESSION['test']=$_GET['test_id'];
$tp=$_GET['test_id'];
$_SESSION['i']=1;
$i=$_SESSION['i'];
$form_location = "questions.php?test_id=".$_SESSION['test'];
?>
<div class="card mb-4"  style="width:60%; margin: 0 auto; padding-bottom:30px;" align=left><!-- / -->
<br><br>
<h1 style="height: bold; font-size: 50px; margin-bottom: -45px; margin-left: 3%; font-family: helvetica; color:#040404;" align=left>New Question<h1>
<hr style="margin-bottom: -50px;">
<form method=post action="question-option_proc.php">
<label style="height: bold; font-size: 30px; margin-bottom: -45px; margin-left: 3%; font-family: helvetica; color:#040404;" align=left>Enter Question</label>
<input class="form-control form-control-user" style="width: 80%; margin-left:3%;" type=text name=q_text autocomplete="off">
<br>

<label style="height: bold; font-size: 25px; margin-bottom: -45px; margin-left: 3%; font-family: helvetica; color:#040404;" align=left>Enter Options</label>
<?php

for ($i=1; $i<=$_SESSION['x']; $i++){
    ?>
    
    <div class="input-group" style="width: 80%; margin-left: 3%">
    <div class="input-group-prepend">
    <div class="input-group-text">
      <input type=radio name=correct value=<?=$i?> autocomplete='off' required>
    </div>
    </div>
        <input class='form-control form-control' style='width: 50%;' type=text name=option-text<?=$i?> autocomplete='off' required>
    </div>
<?php
}
?>
<br>
<button style="margin-left: 3%;" type=submit class="btn btn-success"> ENTER</button>
<a href="questions.php?test_id=<?=$_SESSION['test']?>" class="btn btn-danger btn-icon-split"><span>CANCEL</span></a>
</form>



<br>



<form method=post action="add-option.php?test_id=<?=$tp ?>" style="width:30%; margin-left:5%;">
<div class="input-group mb-3">
  <input type="text" class="form-control"  aria-label="Recipient's username" aria-describedby="button-addon2" name=change autocomplete='off' >
  <div class="input-group-append">
    <button class="btn btn-outline-secondary" id="button-addon2" type=submit name=add>+ OPTIONS</button>
  </div>
</div>
</form>



<form method=post action="minus-option.php?test_id=<?= $tp ?>"style="width:30%; margin-left:5%;">
<div class="input-group mb-3">
  <input type="text" class="form-control"  aria-label="Recipient's username" aria-describedby="button-addon2" name=change autocomplete='off' >
  <div class="input-group-append">
    <button class="btn btn-outline-secondary" id="button-addon2" type=submit name=add>- OPTIONS&nbsp;</button>
  </div>
</div>
</form>
<?php
if (isset($_SESSION['alert_minus_option'])){
    echo "<span style='margin-left:7%;color:red;font-size:18px;'><i>There must be at least 2 options</i></span>";
    unset($_SESSION['alert_minus_option']);
}
// print_r($_SESSION);
?>
</div>
<!-- / -->
    </div>
</div>
    
</body>
</html>