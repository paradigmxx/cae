<?php
session_start();

include "includes/perfect_function.php";
include "includes/database.php";
include "includes/header.php";


$_SESSION['test']=$_GET['test_id'];
$tp=$_GET['test_id'];
$_SESSION['i']=1;
$i=$_SESSION['i'];
$form_location = "questions.php?test_id=".$_SESSION['test'];
?>
<div class="card mb-4"  style="width:60%; margin: 0 auto; padding-bottom:30px;" align=left><!-- / -->
<br><br>
<h1 style="height: bold; font-size: 50px; margin-bottom: -45px; margin-left: 3%; font-family: helvetica; color:#040404;" align=left>New Question<h1>
<hr style="margin-bottom: -50px;">
<form method=post action="question-option_proc.php">
<label style="height: bold; font-size: 30px; margin-bottom: -45px; margin-left: 3%; font-family: helvetica; color:#040404;" align=left>Enter Question</label>
<input class="form-control form-control-user" style="width: 80%; margin-left:3%;" type=text name=q_text autocomplete="off">
<br>

<label style="height: bold; font-size: 25px; margin-bottom: -45px; margin-left: 3%; font-family: helvetica; color:#040404;" align=left>Enter Options</label>



    <div class="input-group" style="width: 80%; margin-left: 3%">
        <div class="input-group-prepend">
        <div class="input-group-text">
            <input type=radio name=correct value='1' required>
        </div>
        </div>
            <input type=text name=option-text1 value='True' readonly>
        </div>
    
        
    <div class="input-group" style="width: 80%; margin-left: 3%">
        <div class="input-group-prepend">
        <div class="input-group-text">
            <input type=radio name=correct value='2' required>
        </div>
        </div>
            <input type=text name=option-text2 value='False' readonly>
        </div>


<br>
<button style="margin-left: 3%;" type=submit class="btn btn-success"> ENTER</button>
<a href="questions.php?test_id=<?=$_SESSION['test']?>" class="btn btn-danger btn-icon-split"><span>CANCEL</span></a>
</form>

</div>
</div>

</body>
</html>