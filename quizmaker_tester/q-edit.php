<?php
session_start();

include "includes/perfect_function.php";
include "includes/database.php";
include "includes/dc_asset.php";
include "includes/header.php";

// __________________________________________________________________________
$id = $_GET['question_id'];
$form_location = "q_edit_proc.php?question_id=".$id;

$table_name = "questions";

function get_where_q($table_name, $id)
{
	$conn = getConnection();
	$sql = "SELECT * FROM $table_name where question_id=$id";
	$result = $conn->query($sql);
	return $result;
}

$get_userData = get_where_q($table_name, $id);
//fetch result and pass it  to an array
foreach ($get_userData as $key => $row) {
	$q_text = $row['q_text'];
	$correct = $row['correct'];
}
?>
<div class="card mb-4"  style="width:60%; margin: 0 auto; padding-bottom:30px;" align=left><!-- / -->
<br><br>
<h1 style="height: bold; font-size: 50px; margin-bottom: -45px; margin-left: 3%; font-family: helvetica; color:#040404;" align=left>Edit Question<h1>
<hr style="margin-bottom: -50px;">

<form method="post" action="<?= $form_location ?>">
<label style="height: bold; font-size: 25px; margin-bottom: -45px; margin-left: 3%; font-family: helvetica; color:#040404;" align=left>Edit Question Text:</label>
	<input type="text" name="q_text" value="<?= $q_text ?>" class="form-control form-control" autocomplete=off required style="width:80%; margin-left:3%; margin-top:1%;"> 
	<label style="height: bold; font-size: 25px; margin-bottom: -45px; margin-left: 3%; margin-top:2%; font-family: helvetica; color:#040404;" align=left>Edit Correct Option</label>
	<input type="number" name="correct" value="<?= $correct ?>" class="form-control form-control-user" autocomplete=off required style="width:80%; margin-left:3%; margin-top:1%;"> 
	
	<br>
	<button style="margin-left: 3%;" type=submit class="btn btn-success"> ENTER</button>
	<a href="questions.php?test_id=<?=$_SESSION['test']?>" class="btn btn-danger btn-icon-split"><span>CANCEL</span></a>
</form>

