<?php
session_start();

include "includes/perfect_function.php";
include "includes/database.php";
include "includes/dc_asset.php";
include "includes/header.php";

if (!isset($_SESSION['username'])){
	header ("Location: logout.php");
	}

// __________________________________________________________________________
$id = $_GET['test_id'];
$form_location = "test_edit_proc.php?test_id=".$id;

$table_name = "tests";

function get_where_test($table_name, $id)
{
	$conn = getConnection();
	$sql = "SELECT * FROM $table_name where test_id=$id";
	$result = $conn->query($sql);
	return $result;
}

$get_userData = get_where_test($table_name, $id);
//fetch result and pass it  to an array
foreach ($get_userData as $key => $row) {
	$testname = $row['testname'];

}
?>


<div class=container>
    <div class=maincontent>

	<div class="card mb-4"  style="width:60%; margin: 0 auto; padding-bottom:30px;" align=left><!-- / -->
<br><br>
<h1 style="height: bold; font-size: 50px; margin-bottom: -45px; margin-left: 3%; font-family: helvetica; color:#040404;" align=left>EDIT TESTNAME<h1>
<hr style="margin-bottom: -50px;">

<form method="post" action="<?= $form_location ?>">

	<input type="text" name="testname" value="<?= $testname ?>" class="form-control form-control" autocomplete=off required style="width:80%; margin-left:3%; margin-top:1%;"> 
	<br>
	<button style="margin-left: 3%;" type=submit class="btn btn-success"> ENTER</button>
	<a href="test.php" class="btn btn-danger btn-icon-split"><span>CANCEL</span></a>
</form>

</div>
</div>

</body>
</html>