
<?php
session_start();

if (!isset($_SESSION['username'])){
    header ("Location: logout.php");
    }
include "includes/header.php";
?>
<div class="card mb-4"  style="width:60%; margin: 0 auto; padding-bottom:30px;"><!-- / -->
<table border=1>
<tr>

    <td>QUESTION</td>
    <td>ANSWER</td>
    <td colspan=2>&nbsp;</td>
<?php
include "includes/perfect_function.php";
include "includes/database.php";


        $id=$_SESSION['id'];
        $test_id=$_GET['test_id'];
        $_SESSION['test']=$test_id;
        $form_location = "add_q.php?test_id=".$test_id;
        $form_location2 = "add_q2.php?test_id=".$test_id;
        $table_name="tests";

        $user_data=get($table_name);
        // a LJ b on b=a where b=c
        $query=mySQLi_query($aVar, "SELECT * from questions LEFT JOIN tests on tests.test_id = questions.test_id where tests.test_id= $test_id order by question_id ASC") or die(mySQLi_error($aVar));
        while($row=mySQLi_fetch_array($query)){
            $question_id=$row['question_id'];
            $test_id=$row['test_id'];
            $q_text=$row['q_text'];
            $correct=$row['correct'];
?>

            <tr>

            <td><a href="options.php?question_id=<?= $question_id?>"><?= $q_text?></a></td>
            <td><?= $correct?></td>
            <td><a href="q-edit.php?question_id=<?=$question_id?>">Edit</a></td>
            <td><a href="q-delete.php?question_id=<?=$question_id?>">Delete</a></td>
            </tr>

<?php
        }

?>
</table>

<br>
<form method="post" action="<?= $form_location ?>">
<div class="input-group" style="width:60%; margin: 0 auto; padding-bottom:30px;"><!-- / -->
  <input type="text" class="form-control" placeholder="ADD QUESTION" aria-label="Recipient's username with two button addons" aria-describedby="button-addon4" readonly>
  <div class="input-group-append" id="button-addon4">
    <button type="submit" class="btn btn-outline-dark">Multiple Choice</button>
    <a href="<?= $form_location2 ?>" class="btn btn-outline-dark"><span>True or False</span></a>
    <a href="test.php"class="btn btn-danger">Back</a>
</form>
</div>
<!-- / -->
    </div>
    </div>

</body>
</html>
