<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Page Title</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src='main.js'></script>
</head>
<body>


<br><br><br>

<div class="card mb-4"  style="width:50%; margin: 0 auto; padding-bottom:30px;" align=left><!-- / -->
<br><br><br><br>
<h1 style="height: bold; font-size: 70px; margin-top: -45px; margin-left: 3%; font-family: helvetica; color:#040404;" align=left><b>SIGN UP</b><h1>
<hr style="margin-bottom: 50px;">

<form method="post" action="signup_proc.php">
    <input type=text placeholder=Username name=username  class="form-control form-control-user" autocomplete=off required style="width:80%; margin-left:3%; margin-top:1%;"> 

    <input type=text placeholder=Firstname name=firstname  class="form-control form-control-user" autocomplete=off required style="width:80%; margin-left:3%; margin-top:1%;"> 

    <input type=text placeholder=Lastname name=lastname  class="form-control form-control-user" autocomplete=off required style="width:80%; margin-left:3%; margin-top:1%;"> 

    <select name="account_type" class="form-control form-control-user" autocomplete=off required style="width:80%; margin-left:3%; margin-top:1%;"> 
        <option value="">Select Account Type:</option>
        <option value="1">Teacher</option>
        <option value="0">Student</option>
        </select>

    <input type=password placeholder=**** name=password class="form-control form-control-user" autocomplete=off required style="width:80%; margin-left:3%; margin-top:1%;"> 


	<button style="margin-left: 3%;" type=submit class="btn btn-success"> ENTER</button>
	<a href="index.php" class="btn btn-danger btn-icon-split"><span>CANCEL</span></a>
</form>
    <?php 
    if (isset($_SESSION['alert_msg'])){
        if ($_SESSION['alert_msg']==2){
            echo "<span style='margin-left: 3%; color: red; font-size: 20px;'><i>Username already taken.</i></span>";
            session_unset();
        }
    }
    ?>
    <br>

</div>
</div>
</body>
</html>