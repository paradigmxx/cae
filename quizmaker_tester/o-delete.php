<?php
session_start();

include "includes/perfect_function.php";
include "includes/database.php";
include "includes/dc_asset.php";
include "includes/header.php";

$option_id=$_GET['option_id'];
?>

<div class="card mb-4"  style="width:60%; margin: 0 auto; padding-bottom:30px;" align=left><!-- / -->
<br><br>
<h1 style="height: bold; font-size: 50px; margin-bottom: -45px; margin-left: 3%; font-family: helvetica; color:#040404;" align=left>Delete Option<h1>
<hr style="margin-bottom: -50px;">
<input type="text" name="q_text" class="form-control form-control" placeholder="Are you sure you want to delete this option?" autocomplete=off required style="width:80%; margin-left:3%; margin-top:-3%;" readonly> 
<br>
<a href="delete_proc_o.php?id=<?= $option_id?>"class="btn btn-danger btn-icon-split" style="margin-left:3%;">Delete</a>

<a href="options.php?question_id=<?=$_SESSION['q']?>" class="btn btn-outline-secondary" style="margin-left:1%;">Cancel</a> 
</div><!-- / -->

